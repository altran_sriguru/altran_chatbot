
module.exports = function(controller) {

  controller.middleware.spawn.use(function(bot, next) {

    
      bot.identity = {
          name: 'Botkit for Web',
          id: 'web',
      }
    
    next();

  })

    if (controller.storage && controller.storage.history) {
        controller.middleware.receive.use(function (bot, message, next) {
            var user = {
                id: message.user
            }
            controller.storage.users.save(user, function (err) {
                next();

            });

        });
    }
}
