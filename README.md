# BotAgent

![](bot-agent.gif)


**Installation**

1. git clone git@bitbucket.org:kiruthigap/altran-chatbot.git
2. npm i
3. npm start


**Docker Installation**

1. docker build -t altran-chatbot .
2. docker run -d -p 3000:3000 altran-chatbot
