
module.exports = function(controller) {


    function conductOnboarding(bot, message) {

      bot.startConversation(message, function(err, convo) {
        convo.say("Hai Gentleman, Welcome!");
        convo.say("How may I help you?");
        convo.say({
          text: 'What kind of assist you need?',
          quick_replies: [
            {
              title: 'Inner Car Issues',
              payload: 'Inner Car Issues',
            },
            {
              title: 'Outer Car Issues',
              payload: 'Outer Car Issues',
            }
          ]
        });

      });
    }

    function unhandledMessage(bot, message) {
      bot.startConversation(message, function(err, convo) {
        convo.say("Feel free to share your thoughts!");
        //convo.say("");
        convo.say('<a href="mailto:sriguru.v@altran.com">Share your queries here..</a>');

      });
    }
      controller.hears(['Inner Car Issues','Outer Car Issues'],'message_received', function(bot, message) {
        bot.startConversation(message, function(err, convo) {
          convo.say({
            text: 'Mention the issue',
            quick_replies: [
              {
                title: 'Break/Accelerator/Clutch Issue',
                payload: 'Break/Accelerator/Clutch Issue',
              },
              {
                title: 'Dashboard Issue',
                payload: 'Dashboard Issue',
              }
            ]
          });
        });
        });
        /*controller.hears(['Outer Car Issues'],'message_received', function(bot, message) {
          bot.startConversation(message, function(err, convo) {
            convo.say({
              text: 'Please',
              quick_replies: [
                {
                  title: 'Break/Accelerator/Clutch Issue',
                  payload: 'Break/Accelerator/Clutch Issue',
                },
                {
                  title: 'Dashboard Issue',
                  payload: 'Dashboard Issue',
                }
              ]
            });
          });
          });*/
        controller.hears(['Break/Accelerator/Clutch Issue'],'message_received', function(bot, message) {
          bot.startConversation(message, function(err, convo) {
            convo.say({
              text: 'Please Contact this Toll Free Number - (18000098)',
            });
          });
          });
    controller.hears('bye','message_received', function(bot, message) {
      bot.startConversation(message, function(err, convo) {
      convo.say({
            text: 'Goodbye friend',
          });
      });
      });

    controller.on('hello', conductOnboarding);
    controller.on('welcome_back', conductOnboarding);
    controller.on('message_received', unhandledMessage);

    controller.on('receive_error', function(err, bot, message, pipeline_stage) {
      bot.reply(message, `There was an error processing your request. Please try again later. Error: ${err.toString()}`);
  });

  
  
  controller.on('normalize_error', function(err, bot, message, pipeline_stage) {
    bot.reply(message, `There was an error processing your request. Please try again later. Error: ${err.toString()}`);
  });
  
  
  controller.on('ingest_error', function(err, bot, message, pipeline_stage) {
    bot.reply(message, `There was an error processing your request. Please try again later. Error: ${err.toString()}`);
  });
    controller.on('categorize_error', function(err, bot, message, pipeline_stage) {
      bot.reply(message, `There was an error processing your request. Please try again later. Error: ${err.toString()}`);
    });
    

  }

