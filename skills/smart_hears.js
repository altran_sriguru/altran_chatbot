rasa = require('../rasa');
module.exports = function(controller) {

    controller.hears(['greet'], 'message_received', rasa.hears, function(bot, message) {
      if (message.intent.confidence >= 0.8){
          bot.reply(message,"Hai! Welcome to RAS Chat!");
          bot.reply(message,{
            text: 'How may I help you?',
            quick_replies: [
              {
                title: 'Resource Allocation',
                payload: 'Resource Allocation',
              },
              {
                title: 'Asset Allocation',
                payload: 'Asset Allocation',
              }
            ]
          });
        }else{
          bot.reply(message,"Sorry! I am not sure what you have typed in..");
          bot.reply(message,"Let's get you to a solution");
          bot.reply(message,'<a href="mailto:sriguru.v@altran.com">Share your queries..</a>');
        }        
    });
    

    controller.hears(['request_id'], 'message_received', rasa.hears, function(bot, message) {
      if (message.intent.confidence >= 0.8){
      bot.reply(message,"The Request ID you have entered is :"+message.entities[0].value);
      controller.storage.requestor_master.get(message.entities[0].value, function(err, requestor_master) {
        console.log(requestor_master);
       
    });
      //bot.reply(message,"Other Allocation Details are given below, "+message.entities[0].value);
      }
      else
      {
        bot.reply(message,"Sorry, I can't able to understand...");
      }   
  });
  controller.hears(['asset_request_id'], 'message_received', rasa.hears, function(bot, message) {
    bot.reply(message,"The Request ID you have entered is :"+message.entities[0].value);    
});

  controller.hears('typing','message_received', function(bot, message) {
    bot.reply(message,{
      text: 'This message used the automatic typing delay',
      typing: true,
    }, function() {
      bot.reply(message,{
        text: 'This message specified a 5000ms typing delay',
        typingDelay: 5000,
      });
    });
  });

}
